let wrapper = document.createElement("div");

const gotCitiesCSV = "King's Landing, Braavos, Volantis, Old Valyria, Free Cities, Qarth, Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1() {
    let ans = JSON.stringify(gotCitiesCSV.split(","));
    writeToDOM("Kata 1", ans);
}

function kata2() {
    let ans = JSON.stringify(bestThing.split(" "));
    writeToDOM("Kata 2", ans);
}

function kata3() {
    let ans = JSON.stringify(gotCitiesCSV.replace(/,/g, ';'));
    writeToDOM("Kata 3", ans);
}

function kata4() {
    let ans = lotrCitiesArray.join(', ')
    writeToDOM("Kata 4", ans)
}

function kata5() {
    let ans = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"].splice(0, 5)
    writeToDOM("Kata 5", ans)
}

function kata6() {
    let ans = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"].splice(3, 5)
    writeToDOM("Kata 6", ans)
}

function kata7() {
    let ans = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"].splice(2, 3)
    writeToDOM("Kata 7", ans)
}

function kata8() {
    let ans = lotrCitiesArray.splice(2, 1)
    writeToDOM("Kata 8", lotrCitiesArray)
}

function kata9() {
    let ans = lotrCitiesArray.splice(-2, 3)
    writeToDOM("Kata 9", lotrCitiesArray)
}

function kata10() {
    lotrCitiesArray.splice(2, 0, "Rohan")
    writeToDOM("Kata 10", lotrCitiesArray)
}

function kata11() {
    lotrCitiesArray.splice(-1, 1, "Deadest Marshes");
    writeToDOM("Kata 11", lotrCitiesArray)
}

function kata12() {
    let ans = bestThing.slice(0, 14)
    writeToDOM("Kata 12", ans)
}
function kata13() {
    let ans = bestThing.slice(-12)
    writeToDOM("Kata 13", ans)
}

function kata14() {
    let ans = bestThing.slice(23, 38);
    writeToDOM("Kata 14", ans)
}

function kata15() {
    let ans = bestThing.substring(69)
    writeToDOM("Kata 15", ans)
}

function kata16() {
    let ans = bestThing.substring(23, 38)
    writeToDOM("Kata 16", ans)
}

function kata17() {
    let ans = bestThing.indexOf("only")
    writeToDOM("Kata 17", ans)
}

function kata18() {
    let ans = bestThing.indexOf("bit")
    writeToDOM("Kata 18", ans)
}

function kata19() {
    let checkcity = (city) => {
        if (city.includes("aa") || city.includes("ee") || city.includes("uu") || city.includes("oo")) {
            return city;
        }
    }
    writeToDOM("Kata19", gotCitiesCSV.split(",").filter(checkcity))
}

function kata20() {
    const arr = [];
    for(let i = 0; i < lotrCitiesArray.length; i++){   
        if(
        lotrCitiesArray[i].endsWith("or")
        )
        arr.push(lotrCitiesArray[i])
    }
    writeToDOM("Kata20", arr)
}

function kata21() {
    const arr =[];
    let ans = bestThing.split(' ')
    for ( let i =0; i < ans.length; i++){
        if(
            ans[i].startsWith("b")){
            arr.push(ans[i])

    }

}
    writeToDOM("Kata21", arr)

}

function kata22() {
        let ans = lotrCitiesArray.includes("Mirkwood")
        writeToDOM("Kata22", ans )}  

function kata23() {
    let ans = lotrCitiesArray.includes("Hollywood")
    writeToDOM("Kata23", ans)
}  
function kata24() {
    let ans = lotrCitiesArray.indexOf("Mirkwood")
    writeToDOM("Kata24", ans)
}  

function kata25(){
    const arr =[];
    for(let i =0; i < lotrCitiesArray.length; i++){
    if( lotrCitiesArray[i].includes(" ")){
     arr.push(lotrCitiesArray[i])
    }
}
writeToDOM("Kata25", arr)
}

function kata26() {
    let ans = lotrCitiesArray.reverse()
    writeToDOM("Kata26", ans)
}
{
function kata27() {
    let ans = lotrCitiesArray.sort()
    writeToDOM("Kata27", ans)
}
function kata28() {
    lotrCitiesArray.sort(function(a, b){
        return b.length - a.length
        
    })
    writeToDOM("Kata28", lotrCitiesArray)
}
function kata29() {
   let ans = lotrCitiesArray.pop()
   writeToDOM("Kata2 9", lotrCitiesArray)
}
function kata30() {
    let ans = lotrCitiesArray.push("Rohan")
    writeToDOM("Kata30", lotrCitiesArray)
}
function kata31() {
    let ans = lotrCitiesArray.shift()
    writeToDOM("Kata31", lotrCitiesArray)
}
function kata32() {
    let ans = lotrCitiesArray.unshift("Deadset Marshes")
    writeToDOM("Kata32", lotrCitiesArray)
}


function writeToDOM(titleText, ans) {
    let div = document.createElement("div");
    let title = document.createElement("h3");
    let header = document.createTextNode(titleText);
    let text = document.createTextNode(ans);
    title.appendChild(header);
    div.appendChild(title);
    div.appendChild(text);
    wrapper.appendChild(div)
}
let destination = document.getElementById("content");
destination.appendChild(wrapper);

kata1();
kata2();
kata3();    
kata4();
kata5();
kata6();
kata7();
kata8();
kata9();
kata10();
kata11();
kata12();
kata13();
kata14();
kata15();
kata16();
kata17();
kata18();
kata19();
kata20();
kata21();
kata22();
kata23();
kata24();
kata25();
kata26();
kata27();
kata28();
kata29();
kata30();
kata31();
kata32();
}